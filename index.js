// Exponent Operator
let cubeNum = 2
let getCube = cubeNum**3 ;

// Template Literals
console.log(`The Cube of ${cubeNum } is ${getCube}`);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

let [address1,address2,address3,address4,] = address;
console.log(`I live at ${address1} ${address2}, ${address3} ${address4}`);

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

let {name,species,weight,measurement} = animal;
console.log(`${name} was a ${species}. He weight at ${weight} kgs with a measurement of ${measurement}.`);

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

let numLoop = numbers.forEach((num) => {
	console.log(num);
})



// Javascript Classes


class Dog{
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}

}

let dog1 = new Dog ("Frankie", 5, "Miniature Dacgshund");
let dog2 = new Dog ("Jamie", 2, "Siberian Husky");

console.log(dog1);
console.log(dog2);




	
